import com.amazonaws.AmazonWebServiceRequest
import com.amazonaws.handlers.AsyncHandler
import scala.concurrent._
import scala.reflect.ClassTag

object DynamoFutureUtils {


  def createHandlerAndFuture[R <: AmazonWebServiceRequest, S]()(implicit _request: ClassTag[R], _result: ClassTag[S]):
  (AsyncHandler[R, S], Future[(R, S)]) = {

   val promise = Promise[(R, S)]()

    val h = new AsyncHandler[R, S] {
      def onError(exception: Exception) {
        println("exception:"  + exception)

        promise.failure(exception)
      }
      def onSuccess(request: R, result: S) {
        println("success" +  (request,result) )

        promise.success((request, result))
      }
    }
    (h, promise.future)
  }

}
