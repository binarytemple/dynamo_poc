

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient
import com.amazonaws.regions.{RegionUtils, Region}
import com.amazonaws.services.dynamodbv2.model._
import org.joda.time.{DateTime, DateMidnight}
import java.util.concurrent.Future
import com.amazonaws.auth.{BasicAWSCredentials, AWSCredentials, AWSCredentialsProvider}

object DynamoScanner extends DynamoT with ReadConfig{

  def main(args: Array[String]) {

    val client = new AmazonDynamoDBAsyncClient(aws)

    val r = RegionUtils.getRegion(region)
    client.setRegion(r)

    import scala.collection.JavaConversions._

    val scanRequest = new ScanRequest().withTableName(tableName)
    val result = client.scan(scanRequest)
    result.getItems.toList.foreach(println)


  }


}




