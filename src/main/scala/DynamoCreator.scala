

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.dynamodbv2.model._
import org.joda.time.{DateMidnight, DateTime}
import java.util.concurrent.Future
import com.amazonaws.auth.{BasicAWSCredentials, AWSCredentials, AWSCredentialsProvider}

object DynamoCreator extends DynamoT with ReadConfig{

  def main(args: Array[String]) {

    val client = new AmazonDynamoDBAsyncClient(aws)

    val r = RegionUtils.getRegion(region)
    client.setRegion(r)

    import scala.collection.JavaConversions._
    val throughput: ProvisionedThroughput = new ProvisionedThroughput().withReadCapacityUnits(1).withWriteCapacityUnits(1)

    val nameKey = new KeySchemaElement().withKeyType(KeyType.HASH).withAttributeName("name")
    val ageKey = new KeySchemaElement().withKeyType(KeyType.RANGE).withAttributeName("age")

    val keys = List(nameKey, ageKey)

    val indexKeySchema = List[KeySchemaElement](
      new KeySchemaElement().withAttributeName("name").withKeyType(KeyType.HASH),
      new KeySchemaElement().withAttributeName("years-remaining").withKeyType(KeyType.RANGE)
    )

    val projection = new Projection().withProjectionType(ProjectionType.ALL)
    val localSecondaryIndex: LocalSecondaryIndex = new LocalSecondaryIndex()
      .withIndexName("years-remaining-index").withKeySchema(
      indexKeySchema: _*

    ).withProjection(projection)

    val request: CreateTableRequest = new CreateTableRequest(tableName, keys)
      .withProvisionedThroughput(throughput)
      .withAttributeDefinitions(
      new AttributeDefinition().withAttributeName("name").withAttributeType("S"),
      new AttributeDefinition().withAttributeName("age").withAttributeType("N"),
      new AttributeDefinition().withAttributeName("years-remaining").withAttributeType("N")
    )
      .withLocalSecondaryIndexes(
      localSecondaryIndex
    )

    try {
      val res: Future[CreateTableResult] = client.createTableAsync(request)
      println("CREATED>>>%s".format(res.get()))
    } catch {
      case t: Throwable => System.err.println(s"CREATE>>$t")
    }
  }
}
