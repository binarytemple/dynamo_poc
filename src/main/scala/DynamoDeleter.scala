import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.dynamodbv2.model._
import com.amazonaws.auth.{BasicAWSCredentials, AWSCredentials, AWSCredentialsProvider}

object DynamoDeleter extends DynamoT with ReadConfig{

  def main(args: Array[String]) {
    val client = new AmazonDynamoDBAsyncClient(aws)
    val r = RegionUtils.getRegion(region)
    client.setRegion(r)
    import scala.collection.JavaConversions._
    val ltr: ListTablesResult = client.listTables()
    val tables = ltr.getTableNames.toList.withFilter(_.contains("bryan"))
    tables.foreach {
      t =>
        client.deleteTable(new DeleteTableRequest(t))
    }
  }
}
