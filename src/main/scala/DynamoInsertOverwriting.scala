import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.dynamodbv2.model._
import java.util.UUID
import org.joda.time.DateTime

object DynamoInsertOverwriting extends DynamoT with ReadConfig{

  def main(args: Array[String]) {

    val client = new AmazonDynamoDBAsyncClient(aws)

    val r = RegionUtils.getRegion(region)
    client.setRegion(r)

    import scala.collection.JavaConversions._

    val data = Map(
      "name" -> new AttributeValue("bryan"),
      "age" -> new AttributeValue().withN(39.toString),
      "years-remaining" -> new AttributeValue().withN(10.toString),
      "counter" -> new AttributeValue().withN(0.toString),
      "date" -> new AttributeValue().withS(new DateTime().toString)
    )
    client.putItem(new PutItemRequest(tableName, data))
  }
}
