import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.ClientConfiguration
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient

trait DynamoT {
  self: ReadConfig =>
  def aws:AWSCredentialsProvider
  lazy val client = new AmazonDynamoDBAsyncClient(aws,new ClientConfiguration().withSocketTimeout(1000).withMaxErrorRetry(2))
}
