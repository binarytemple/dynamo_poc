import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.dynamodbv2.model._
import java.util.UUID
import org.joda.time.DateMidnight

object DynamoRetrieve extends DynamoT with ReadConfig{

  def main(args: Array[String]) {

    val client = new AmazonDynamoDBAsyncClient(aws)

    val r = RegionUtils.getRegion(region)
    client.setRegion(r)

    import scala.collection.JavaConversions._

    val keyConditions = Map(
      "age" -> new Condition().withComparisonOperator("EQ").withAttributeValueList(new AttributeValue().withN("39")),
      "name" -> new Condition().withComparisonOperator("EQ").withAttributeValueList(new AttributeValue("bryan"))
    )

    val qr = new QueryRequest().
      withTableName(tableName).
      withAttributesToGet("name","age","years-remaining","counter").
      withKeyConditions(
        keyConditions
      )
    val res: QueryResult = client.query(qr)

    System.err.println(res)

  }
}
