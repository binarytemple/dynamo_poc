import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.dynamodbv2.model._
import java.util.UUID
import org.apache.commons.lang.math.RandomUtils
import org.joda.time.DateMidnight
import java.util.concurrent.Future
import com.amazonaws.auth.{BasicAWSCredentials, AWSCredentials, AWSCredentialsProvider}

object DynamoInsert extends DynamoT with ReadConfig {

  def main(args: Array[String]) {

    val client = new AmazonDynamoDBAsyncClient(aws)

    val r = RegionUtils.getRegion(region)
    client.setRegion(r)

    import scala.collection.JavaConversions._

    val data = Map(
      "name" -> new AttributeValue("bryan" + UUID.randomUUID().toString),
      "age" -> new AttributeValue().withN(39.toString),
      "years-remaining" -> new AttributeValue().withN(10.toString),
      "counter" -> new AttributeValue().withN(0.toString),
      "misc" -> new AttributeValue().withS("a miscellaneous value")
    )
    client.putItem(new PutItemRequest(tableName, data))
  }
}
