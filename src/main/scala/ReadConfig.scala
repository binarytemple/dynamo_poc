import com.amazonaws.auth.{BasicAWSCredentials, AWSCredentials, AWSCredentialsProvider}
import java.io.File
import org.apache.commons.configuration.PropertiesConfiguration
import org.joda.time.DateMidnight

trait ReadConfig {

  val props = new PropertiesConfiguration(new File(System.getProperty("user.home") + File.separator + "dynamo.properties"))
  val region = props.getString("region", "eu-west-1")

  val tableName = {
    val time = new DateMidnight().getMillis.toString
    //val time = new DateTime().getMillis.toString
    val tableName: String = s"bryan_test_table"
    tableName
  }

  def aws = new AWSCredentialsProvider {
    def refresh() {}

    def getCredentials: AWSCredentials = {
      val aws_access_key_id = props.getString("aws_access_key_id")
      val aws_secret_access_key = props.getString("aws_secret_access_key")
      new BasicAWSCredentials(aws_access_key_id, aws_secret_access_key)
    }
  }

}
