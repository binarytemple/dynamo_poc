import java.util.concurrent.{Future => JFuture, Executors}
import scala.concurrent.duration._
import scala.concurrent._

object FutureMainTest {

  def main(args: Array[String]) {

    val tp = Executors.newFixedThreadPool(5)
    implicit val ec = ExecutionContext.fromExecutor(tp)

    def doOp: Future[String] = {
      val promise = Promise[String]()
      tp.submit(
        new Runnable {
          def run() {
            Thread.sleep(1000)
            if ( /*RandomUtils.nextBoolean()*/ false)
              promise.success("foobar")
            else
              promise.failure(new UnsupportedOperationException)
          }
        }
      )
      promise.future
    }

    val chained = doOp recover {
      case t: Throwable => "an exception happened:" + t
    }

    println(Await.result(chained, 10 seconds))


  }

}
