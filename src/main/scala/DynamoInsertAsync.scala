import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.dynamodbv2.model._
import java.util.UUID
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object DynamoInsertAsync extends DynamoT with ReadConfig {

  import DynamoFutureUtils._

  import scala.concurrent.ExecutionContext.Implicits._


  def main(args: Array[String]) {

    def foo(): Future[(PutItemRequest, PutItemResult)] = {
      val client = new AmazonDynamoDBAsyncClient(aws)

      val r = RegionUtils.getRegion(region)
      client.setRegion(r)

      import scala.collection.JavaConversions._

      val data = Map(
        "name" -> new AttributeValue("bryan" + UUID.randomUUID().toString),
        "age" -> new AttributeValue().withN(39.toString),
        "years-remaining" -> new AttributeValue().withN(10.toString),
        "counter" -> new AttributeValue().withN(0.toString),
        "misc" -> new AttributeValue().withS("a miscellaneous value")
      )

      val h = createHandlerAndFuture[PutItemRequest, PutItemResult]()

      client.putItemAsync(new PutItemRequest(tableName, data), h._1)
      h._2
    }

    val foo1: Future[(PutItemRequest, PutItemResult)] = foo()


    System.err.println(foo1)

    val chained = foo1.map {
      x =>
        x._2
    }

    System.err.println(Await.result(chained, 10 seconds))


  }
}
